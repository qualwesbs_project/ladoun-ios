//
//  AppDelegate.swift
//  Ladoun
//
//  Created by qw on 18/01/21.
//

import UIKit

var bundle = Bundle(path: Bundle.main.path(forResource: (Singleton.shared.language == "ar" ? Singleton.shared.language : "Base"), ofType: "lproj")!)


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window?.backgroundColor = .white
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // For Payfort
    func applicationDidEnterBackground(_ application: UIApplication)
    {
        var bgTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier(rawValue: 0)
    bgTask = application.beginBackgroundTask(expirationHandler: {
    application.endBackgroundTask(bgTask)
        bgTask = UIBackgroundTaskIdentifier.invalid
    })
    }


}

