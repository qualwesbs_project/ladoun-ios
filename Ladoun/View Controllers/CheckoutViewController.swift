//
//  CheckoutViewController.swift
//  Ladoun
//
//  Created by qw on 28/01/21.
//

import UIKit


class CheckoutViewController: UIViewController, SelectFromPicker {
    func selectedItem(name: String) {
        if(currentCountryPicker == 1){
            self.countryField.text = name
            self.selectedCountry = self.countryData.filter{
                $0.country_name == name
            }[0]
            if(self.selectedCountry.id == "AE"){
                self.cityView.isHidden = false
                APICallingViewController.shared.getCities { (val) in
                    self.cityData = val
                }
            }else {
                self.cityView.isHidden = true
            }
        }else if(currentCountryPicker == 2) {
            self.shippingCountryTextfield.text = name
            self.selectedCountry = self.countryData.filter{
                $0.country_name == name
            }[0]
            if(self.selectedCountry.id == "AE"){
                self.shippingCityView.isHidden = false
                APICallingViewController.shared.getCities { (val) in
                    self.cityData = val
                }
            }else {
                self.shippingCityView.isHidden = true
            }
        }else if(currentCountryPicker == 3){
            self.cityField.text = name
        }else if(currentCountryPicker == 4){
            self.shippingCityTextfield.text = name
        }
    }
    
    
    //MARK: IBOutlets
    //Order Summary
    @IBOutlet weak var checkoutHeadingLabel: DesignableUILabel!
    
    @IBOutlet weak var cartTable: ContentSizedTableView!
    
    @IBOutlet weak var orderSummaryDownArrow: UIImageView!
    @IBOutlet weak var orderSummaryView: UIView!
    @IBOutlet weak var discountCode: UITextField!
    
    @IBOutlet weak var cartTotalTax: DesignableUILabel!
    @IBOutlet weak var cartTotalPrice: DesignableUILabel!
    @IBOutlet weak var CartQuantity: DesignableUILabel!
    @IBOutlet weak var cartTotalAmount: DesignableUILabel!
    @IBOutlet weak var cartSubtotal: DesignableUILabel!
    @IBOutlet weak var cartTotal: DesignableUILabel!
    @IBOutlet weak var cartGrandTotal: DesignableUILabel!
    
    
    //Account & Billings
    @IBOutlet weak var accountBillingDownarrow: UIImageView!
    @IBOutlet weak var accountBillingView: UIView!
    @IBOutlet weak var emailYesImage: ImageView!
    @IBOutlet weak var emailNoImage: ImageView!
    @IBOutlet weak var isEmailLabel: DesignableUILabel!
    @IBOutlet weak var yesLabel: UILabel!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var emailPlaceholder: DesignableUILabel!
    @IBOutlet weak var emailTextfield: DesignableUITextField!
    @IBOutlet weak var fullnamePlaceholder: DesignableUILabel!
    @IBOutlet weak var fullnameTextfield: DesignableUITextField!
    @IBOutlet weak var countryPlaceholder: DesignableUILabel!
    @IBOutlet weak var countryField: DesignableUITextField!
    @IBOutlet weak var cityPlaceholder: DesignableUILabel!
    @IBOutlet weak var cityField: DesignableUITextField!
    @IBOutlet weak var streetPlaceholder: DesignableUILabel!
    @IBOutlet weak var streetField: DesignableUITextField!
    @IBOutlet weak var mobileNumberPlaceholder: DesignableUILabel!
    @IBOutlet weak var mobileNumberField: DesignableUITextField!
    @IBOutlet weak var addressSameImage: ImageView!
    @IBOutlet weak var differentAddressImage: ImageView!
    @IBOutlet weak var shippingDetailView: UIView!
    @IBOutlet weak var shippingDetailStack: UIStackView!
    @IBOutlet weak var stateDownarrowImage: UIImageView!
    @IBOutlet weak var stateTextField: DesignableUITextField!
    @IBOutlet weak var stateLabel: DesignableUILabel!
    
    
    
    //ShippingDetailView
    @IBOutlet weak var shippingDetailDropdownImage: UIImageView!
    @IBOutlet weak var shippingfullnamePlaceholder: DesignableUILabel!
    @IBOutlet weak var shippingFullnameTextfield: DesignableUITextField!
    @IBOutlet weak var shippingCountryPlaceholder: DesignableUILabel!
    @IBOutlet weak var shippingCountryTextfield: DesignableUITextField!
    @IBOutlet weak var shippingCityPlaceholder: DesignableUILabel!
    @IBOutlet weak var shippingCityTextfield: DesignableUITextField!
    @IBOutlet weak var shippingStreetAddress: DesignableUILabel!
    @IBOutlet weak var shippingAddressField: DesignableUITextField!
    @IBOutlet weak var shippingNumberPlaceholder: DesignableUILabel!
    @IBOutlet weak var shippingNumberField: DesignableUITextField!
    @IBOutlet weak var shippingCityView: UIView!
    @IBOutlet weak var cityView: UIView!
    
    
    
    //PaymentMethodView
    @IBOutlet weak var paymentMethodView: UIView!
    @IBOutlet weak var paymentMethodDropdownArrow: UIImageView!
    @IBOutlet weak var codImage: ImageView!
    @IBOutlet weak var codLabel: UILabel!
    @IBOutlet weak var creditDebitImage: ImageView!
    @IBOutlet weak var creditDebitLabel: UILabel!
    @IBOutlet weak var cardDetailStack: UIStackView!
    @IBOutlet weak var cardholderName: DesignableUITextField!
    @IBOutlet weak var cardNumber: DesignableUITextField!
    @IBOutlet weak var expiryMonth: DesignableUITextField!
    @IBOutlet weak var expiryYear: DesignableUITextField!
    @IBOutlet weak var cvvField: DesignableUITextField!
    @IBOutlet weak var remarkView: UITextView!
    @IBOutlet weak var invoiceshipmentImage: ImageView!
    
    //Shipping Method View
    @IBOutlet weak var shippingMethodImage: UIImageView!
    @IBOutlet weak var shippingMethodLabel: DesignableUILabel!
    @IBOutlet weak var shipmentTable: ContentSizedTableView!
    @IBOutlet weak var shipmentMethodView: UIView!
    
    
    var cartTotalDetail = GetCart()
    var cartDetail = GetCart()
    var countryData = [CountryData]()
    var cityData = [CityData]()
    var selectedCountry = CountryData()
    var currentCountryPicker = Int()
    var isAddressSame = true
    var estimateShippingData = [GetEstimateShipping]()
    let request = NSMutableDictionary.init()
    private var payfortData = PayfortResponse()
    var selectedShipping = GetEstimateShipping()
    
    let payFort = PayFortController.init(enviroment: KPayFortEnviromentSandBox)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APICallingViewController.shared.getCartTotal{ (val) in
            if((val.items?.count ?? 0) > 0){
                self.cartTotalDetail = val
                self.cartSubtotal.text = (self.cartTotalDetail.base_currency_code ?? "") + " \(self.cartTotalDetail.subtotal ?? 0)"
            }
        }
        APICallingViewController.shared.getCart{ (val) in
            if((val.items?.count ?? 0) > 0){
                self.cartDetail = val
            }
        }
        
        APICallingViewController.shared.getCountries { (val) in
            self.countryData = val
        }
        
        initView()
    }
    
    func initView(){
        initialzePayfort()
        self.mobileNumberField.delegate = self
        self.shippingNumberField.delegate = self
        if((self.cartTotalDetail.items?.count ?? 0) > 0){
            self.cartTable.reloadData()
            self.cartTotalPrice.text = "\(self.cartTotalDetail.currency?.global_currency_code ?? "SAR") " +  "\(self.cartTotalDetail.base_subtotal ?? 0)"
            self.cartTotalTax.text = "\(self.cartTotalDetail.currency?.global_currency_code ?? "SAR") " + "\(self.cartTotalDetail.tax_amount ?? 0)"
            self.CartQuantity.text = "\(self.cartTotalDetail.currency?.global_currency_code ?? "SAR") " + "\(self.cartTotalDetail.items_count ?? 0)"
            self.cartTotalAmount.text = "\(self.cartTotalDetail.currency?.global_currency_code ?? "SAR") " + "\(self.cartTotalDetail.subtotal_incl_tax ?? 0)"
            self.cartTotal.text = "\(self.cartTotalDetail.currency?.global_currency_code ?? "SAR") " + "\(self.cartTotalDetail.subtotal_incl_tax ?? 0)"
            self.cartGrandTotal.text = "\(self.cartTotalDetail.currency?.global_currency_code ?? "SAR") " + "\(self.cartTotalDetail.subtotal_incl_tax ?? 0)"
        }
        //  self.checkoutHeadingLabel.text =
    }
    
    func initialzePayfort() {
        //        Merchant Identifier: hoPmlXqu
        //        Access Code: Xqnr8iYVhFs9pgc4T0N3
        //        Hash Algorithm: SHA-256
        //        Request SHA phrase: jxvhz
        //        Response SHA phrase: dcdcsdvfd
        guard let payFortController = PayFortController(enviroment: KPayFortEnviromentSandBox) else { return }

        let udid  = payFort?.getUDID()//payFortController.getUDID()!
        let algorithem = "jxvhzaccess_code=Xqnr8iYVhFs9pgc4T0N3device_id=\(udid ?? "")language=enmerchant_identifier=hoPmlXquservice_command=SDK_TOKENjxvhz"
        let signature = Encryption.sha256Hex(string: algorithem)
        let param:[String:Any] = [
            "service_command":"SDK_TOKEN",
            "access_code":"Xqnr8iYVhFs9pgc4T0N3",
            "merchant_identifier":"hoPmlXqu",
            "language":"en",
            "device_id":udid ?? "",
            "signature":signature
        ]
        SessionManager.shared.methodForApiCalling(url:"https://sbpaymentservices.payfort.com/FortAPI/paymentApi" , method: .post, parameter: param, objectClass: PayfortResponse.self, requestCode: U_LOGIN, userToken: nil) { (response) in
            self.payfortData = response
        }
    }
    
    //MARK: IBACtions
    @IBAction func discountCodeAction(_ sender: Any) {
    }
    
    @IBAction func orderSummaryAction(_ sender: Any) {
        if(self.orderSummaryView.isHidden){
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveLinear, animations: {
                self.orderSummaryView.isHidden = false
                self.orderSummaryDownArrow.transform = CGAffineTransform(rotationAngle: 180.0 * 3.14/180.0)
            }, completion: nil)
            
        }else {
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveLinear, animations: {
                self.orderSummaryView.isHidden = true
                self.orderSummaryDownArrow.transform = CGAffineTransform(rotationAngle: (0 * 3.14/180.0))
            }, completion: nil)
            
        }
    }
    
    @IBAction func accountBillingAction(_ sender: Any) {
        if(self.accountBillingView.isHidden){
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.accountBillingView.isHidden = false
                self.accountBillingDownarrow.transform = CGAffineTransform(rotationAngle: 180.0 * 3.14/180.0)
            }, completion: nil)
            
        }else {
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.accountBillingView.isHidden = true
                self.accountBillingDownarrow.transform = CGAffineTransform(rotationAngle: (0 * 3.14/180.0))
            }, completion: nil)
            
        }
    }
    
    @IBAction func shippingDetailAction(_ sender: Any) {
        if(self.shippingDetailView.isHidden){
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.shippingDetailView.isHidden = false
                self.shippingDetailDropdownImage.transform = CGAffineTransform(rotationAngle: 180.0 * 3.14/180.0)
            }, completion: nil)
            
        }else {
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.shippingDetailView.isHidden = true
                self.shippingDetailDropdownImage.transform = CGAffineTransform(rotationAngle: (0 * 3.14/180.0))
            }, completion: nil)
            
        }
    }
    
    @IBAction func paymentMethodAction(_ sender: Any) {
        if(self.paymentMethodView.isHidden){
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.paymentMethodView.isHidden = false
                self.paymentMethodDropdownArrow.transform = CGAffineTransform(rotationAngle: 180.0 * 3.14/180.0)
            }, completion: nil)
            
        }else {
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.paymentMethodView.isHidden = true
                self.paymentMethodDropdownArrow.transform = CGAffineTransform(rotationAngle: (0 * 3.14/180.0))
            }, completion: nil)
            
        }
    }
    
    
    @IBAction func emailYesAction(_ sender: Any) {
        emailYesImage.image = #imageLiteral(resourceName: "radioOn")
        emailNoImage.image = #imageLiteral(resourceName: "radioOff")
    }
    
    @IBAction func emailNoAction(_ sender: Any) {
        emailNoImage.image = #imageLiteral(resourceName: "radioOn")
        emailYesImage.image = #imageLiteral(resourceName: "radioOff")
    }
    
    @IBAction func countryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.countryData{
            myVC.pickerData.append(val.country_name ?? "")
        }
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.currentCountryPicker = 1
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func shippingCountryAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.countryData{
            myVC.pickerData.append(val.country_name ?? "")
        }
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.currentCountryPicker = 2
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func cityAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.cityData{
            myVC.pickerData.append(val.code ?? "")
        }
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.currentCountryPicker = 3
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func shippingCityAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        for val in self.cityData{
            myVC.pickerData.append(val.code ?? "")
        }
        myVC.pickerDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        if(myVC.pickerData.count > 0){
            self.currentCountryPicker = 4
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func addressSameAction(_ sender: Any) {
        addressSameImage.image = #imageLiteral(resourceName: "radioOn")
        differentAddressImage.image = #imageLiteral(resourceName: "radioOff")
        self.shippingDetailStack.isHidden = true
        self.isAddressSame = true
    }
    
    @IBAction func differentAddressAction(_ sender: Any) {
        differentAddressImage.image = #imageLiteral(resourceName: "radioOn")
        addressSameImage.image = #imageLiteral(resourceName: "radioOff")
        self.shippingDetailStack.isHidden = false
        self.isAddressSame = false
    }
    
    @IBAction func codAction(_ sender: Any) {
        codImage.image = #imageLiteral(resourceName: "radioOn")
        creditDebitImage.image = #imageLiteral(resourceName: "radioOff")
        self.cardDetailStack.isHidden = true
    }
    
    
    @IBAction func creditDebitAction(_ sender: Any) {
        creditDebitImage.image = #imageLiteral(resourceName: "radioOn")
        codImage.image = #imageLiteral(resourceName: "radioOff")
        self.cardDetailStack.isHidden = false
        
        payFort?.hideLoading = false;
        payFort?.isShowResponsePage = true;
        
        request.setValue("1000", forKey: "amount")
        request.setValue("AUTHORIZATION", forKey: "command")
        request.setValue("USD", forKey: "currency")
        request.setValue("email@domain.com", forKey: "customer_email")
        request.setValue("en", forKey: "language")
        request.setValue(self.payfortData.merchant_identifier ?? "", forKey: "merchant_reference")
        request.setValue(self.payfortData.sdk_token ?? "" , forKey: "sdk_token")
        
        
        payFort?.callPayFort(withRequest: request, currentViewController: self,
                             success: { (requestDic, responeDic) in
                                print("success")
                                print("responeDic=\(responeDic)")
                                print("responeDic=\(responeDic)")
                             },canceled: { (requestDic, responeDic) in
                                print("canceled")
                                print("requestDic=\(requestDic)")
                                print("responeDic=\(responeDic)")
                             },
                             faild: { (requestDic, responeDic, message) in
                                print("faild")
                                print("requestDic=\(requestDic)")
                                print("responeDic=\(responeDic)")
                                print("message=\(message)")
                             })
    }
    
    @IBAction func invoiceAction(_ sender: Any) {
        if(invoiceshipmentImage.image == #imageLiteral(resourceName: "radioOn")){
            invoiceshipmentImage.image = #imageLiteral(resourceName: "radioOff")
        }else {
            invoiceshipmentImage.image = #imageLiteral(resourceName: "radioOn")
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shippingMethodAction(_ sender: Any) {
        if(self.shipmentMethodView.isHidden){
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.shipmentMethodView.isHidden = false
                self.shippingMethodImage.transform = CGAffineTransform(rotationAngle: 180.0 * 3.14/180.0)
            }, completion: nil)
            
        }else {
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                self.shipmentMethodView.isHidden = true
                self.shippingMethodImage.transform = CGAffineTransform(rotationAngle: (0 * 3.14/180.0))
            }, completion: nil)
        }
    }
    
    @IBAction func getshippingMethodAction(_ sender: Any) {
        if(emailTextfield.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter email address")
        }else  if !(self.isValidEmail(emailStr: self.emailTextfield.text ?? "")){
            Singleton.shared.showToast(text: "Enter valid email address")
        }else if(fullnameTextfield.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Full Name")
        }else if(countryField.text!.isEmpty){
            Singleton.shared.showToast(text: "Select country")
        }else if(stateTextField.text!.isEmpty){
            Singleton.shared.showToast(text: "Select state")
        }else if(cityField.text!.isEmpty){
            Singleton.shared.showToast(text: "Select city")
        }else if(streetField.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Street Address")
        }else {
            var components = self.isAddressSame ? self.fullnameTextfield.text!.components(separatedBy: " "): self.shippingFullnameTextfield.text!.components(separatedBy: " ")
            var fName = ""
            var lName = ""
            if components.count > 0 {
                fName = components.removeFirst()
                lName = components.joined(separator: " ")
            }
            let param:[String:Any] = [
                "quote_id":self.cartDetail.id ?? 0,
                "email":self.emailTextfield.text ?? "",
                "shipping_firstname":fName,
                "shipping_lastname":lName,
                "shipping_country":self.isAddressSame ? self.countryField.text ?? "":shippingCountryTextfield.text ?? "",
                "shipping_state":self.isAddressSame ? self.stateTextField.text ?? "":"NA",
                "shipping_city": self.isAddressSame ? self.cityField.text ?? "":self.shippingCityTextfield.text ?? "",
                "shipping_addressline0": self.isAddressSame ? self.streetField.text ?? "":self.shippingStreetAddress.text ?? "",
                "shipping_postcode":0000,
                "shipping_addressline1":"",
                "shipping_region_id":"",
                "shipping_telephone": self.isAddressSame ? self.mobileNumberField.text ?? "":self.shippingNumberField.text ?? "",
                "same_as_billing": self.isAddressSame ? 1:0,
                "action": "estimateshipping",
            ]
            APICallingViewController.shared.getShippingMethod(param: param) { (val) in
                self.estimateShippingData = val
                self.shipmentTable.reloadData()
            }
        }
    }
    
}

extension CheckoutViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == cartTable){
            return self.cartTotalDetail.items?.count ?? 0
        }else if(tableView == shipmentTable){
            return self.estimateShippingData.count ?? 0
        }else{
            return self.estimateShippingData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTable") as! CartTable
        if(tableView == cartTable){
            let val = self.cartTotalDetail.items?[indexPath.row]
            cell.itemImage.sd_setImage(with: URL(string: "https://ladoun.com/pub/media/wysiwyg/pexels-photo-13426091599233514.jpg"), placeholderImage: nil)
            // cell.itemName.text = val?.name
            cell.itemId.text = "\(val?.item_id ?? 0)"
            cell.itemExPrice.text = "\(val?.price ?? 0)"
            cell.itemPrice.text = "\(val?.price_incl_tax ?? 0)"
            cell.itemQuantity.text = "\(val?.qty ?? 0)"
            cell.itemTax.text = "\(val?.tax_amount ?? 0)"
        }else if(tableView == self.shipmentTable) {
            let val = self.estimateShippingData[indexPath.row]
            cell.itemName.text = val.carrier_title
            if(self.selectedShipping.carrier_title == val.carrier_title){
                cell.itemImage.image = #imageLiteral(resourceName: "radioOn")
            }else{
                cell.itemImage.image = #imageLiteral(resourceName: "radioOff")
            }
            cell.plusButton = {
                self.selectedShipping = val
                self.shipmentTable.reloadData()
            }
        }
        return cell
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == mobileNumberField || textField == shippingNumberField){
            if let char = string.cString(using: String.Encoding.utf8) {
                let isBackSpace = strcmp(char, "\\b")
                if (isBackSpace == -92 || textField.text!.count <= 10) {
                    return true
                }else {
                    return false
                }
            }
        }
        return true
    }
}

struct Encryption {
    static func sha256Hex(string: String) -> String? {
        guard let messageData = string.data(using: String.Encoding.utf8) else { return nil }
        var digestData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_SHA256(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData.map { String(format: "%02hhx", $0) }.joined()
    }
    
    static func ccSha256(data: Data) -> Data {
        var digest = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        
        _ = digest.withUnsafeMutableBytes { (digestBytes) in
            data.withUnsafeBytes { (stringBytes) in
                CC_SHA256(stringBytes, CC_LONG(data.count), digestBytes)
            }
        }
        return digest
    }
}

