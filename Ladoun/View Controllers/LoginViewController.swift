//
//  LoginViewController.swift
//  Ladoun
//
//  Created by qw on 02/02/21.
//

import UIKit

class LoginViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var customerloginHeading: DesignableUILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customerloginHeading.text  = NSLocalizedString("Customer Login", tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    //MARK: IBActions
    @IBAction func forgetPassAction(_ sender: Any) {
        if Singleton.shared.getCurrentLanguage() == "ar" {
          //  Singleton.shared.changeApplicationLanguage(language: "en")
          //  UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
           // Singleton.shared.changeApplicationLanguage(language: "ar")
          //  UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
        
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CreateAccountViewController") as! CreateAccountViewController
        self.navigationController?.pushViewController(myVC, animated: true)
        
    }
    
}
