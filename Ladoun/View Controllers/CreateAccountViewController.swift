//
//  CreateAccountViewController.swift
//  Ladoun
//
//  Created by qw on 17/02/21.
//

import UIKit

class CreateAccountViewController: UIViewController {
    //MARK: IBOutlets
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    //MARK: IBActions
    
    @IBAction func createAccountAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
}
