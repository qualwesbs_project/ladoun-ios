//
//  HomeViewController.swift
//  Ladoun
//
//  Created by qw on 17/02/21.
//

import UIKit
import SDWebImage

class HomeViewController: UIViewController, DismissController {
    func dismissView() {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "CheckoutViewController") as! CheckoutViewController
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var itemCollection: UICollectionView!
    
    
    var categoryData = [ChildrenData]()
    var selectedCategory: Int?
    var productData = [ProductData]()
    
    let image = [
        CategoryData(name: "Watches", image: "https://ladoun.com/pub/media/wysiwyg/pexels-photo-13426091599233514.jpg"),
//        CategoryData(name: "Ladies", image: "https://ladoun.com/pub/media/wysiwyg/jutta-wilms-a0r2nyk31SE-unsplash_1_1599233495.jpg"),
//        CategoryData(name: "Gentlemen", image: "https://ladoun.com/pub/media/wysiwyg/pexels-photo-13426091599233514.jpg"),
        CategoryData(name: "Sale", image: "https://ladoun.com/pub/media/wysiwyg/pexels-polina-tankilevitch-3905874_1_1_1599234234.jpg"),
        CategoryData(name: "Shoes", image: "https://ladoun.com/pub/media/wysiwyg/pexels-mstudio-12408921611719875.jpg"),
        CategoryData(name: "Bracelet", image: "https://ladoun.com/pub/media/wysiwyg/shutterstock_521355382_1_1599233512.jpg"),
      
        CategoryData(name: "Cuffinks", image: "https://ladoun.com/pub/media/wysiwyg/shutterstock_1233776317_1_1599233516.jpg"),
        CategoryData(name: "Pens", image: "https://ladoun.com/pub/media/wysiwyg/shutterstock_1233776317_1_1599233516.jpg"),
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicator.show(view: self.view)
        APICallingViewController.shared.getCategories { (response) in
            self.categoryData = response.children_data.filter{
                $0.name != "sets"
            }
            self.categoryCollection.reloadData()
            self.itemCollection.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getSubcategoryData(id:Int){
        let param:[String:Any] = [
            "action": "getallproduct",
             "cat_id" : id
        ]
        SessionManager.shared.methodForApiCalling(url: U_GET_CATEGORIES, method: .post, parameter: param, objectClass: DecodedArray.self  , requestCode: U_GET_CATEGORIES, userToken: nil) { (response) in
            self.productData = []
            for val in response {
                self.productData.append(val)
            }
            self.itemCollection.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func cartAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "MyCartViewController") as! MyCartViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.dismissDelegate = self
        self.present(myVC, animated: true, completion: nil)
    }
    

}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == itemCollection){
            if(self.selectedCategory == nil){
                self.categoryData.count
            }else {
              return self.productData.count
            }
        }else {
         return self.categoryData.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == itemCollection){
            if(self.selectedCategory == nil){
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollection1", for: indexPath) as! CategoryCollection
                cell.categoryName.text = self.categoryData[indexPath.row].name
                cell.categoryImage.sd_setImage(with: URL(string: self.image[indexPath.row].image!), placeholderImage: nil)
                return cell
            }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollection2", for: indexPath) as! CategoryCollection
                let val = self.productData[indexPath.row]
                
            cell.categoryName.text = self.categoryData[indexPath.row].name
                cell.categoryImage.sd_setImage(with: URL(string: val.image ?? ""), placeholderImage: nil)
                cell.categoryPrice.text = "SAR \(val.price ?? "")"
                cell.taxLael.text = "(Price incl. taxes)"
                
            return cell
            }
        }else {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCollection", for: indexPath) as! CategoryCollection
            cell.categoryName.text = self.categoryData[indexPath.row].name
        cell.categoryImage.sd_setImage(with: URL(string: self.image[indexPath.row].image!), placeholderImage: nil)
        return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == itemCollection){
            if(self.selectedCategory == nil){
              return CGSize(width: self.view.frame.width, height:220)
            }else {
                return CGSize(width: collectionView.frame.width/2-5, height:250)
            }
        }else {
            let label = UILabel(frame: CGRect.zero)
            label.text = self.categoryData[indexPath.row].name
            label.sizeToFit()
            
            return CGSize(width: label.frame.width+20, height: 100)
           
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == categoryCollection){
            self.selectedCategory = self.categoryData[indexPath.row].id
            self.getSubcategoryData(id:self.selectedCategory ?? 0)
        }else {
            
        }
    }
    
}

class CategoryCollection: UICollectionViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var categoryImage: ImageView!
    @IBOutlet weak var categoryName: DesignableUILabel!
    @IBOutlet weak var categoryPrice: DesignableUILabel!
    @IBOutlet weak var taxLael: DesignableUILabel!
    @IBOutlet weak var addCartButtton: CustomButton!
    
    var addButton:(()-> Void)? = nil
    
    //MARK: IBActions
    @IBAction func addAction(_ sender: Any) {
        if let addButton = self.addButton {
            addButton()
        }
    }
    
    
}
