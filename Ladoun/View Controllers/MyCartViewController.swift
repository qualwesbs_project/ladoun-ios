//
//  MyCartViewController.swift
//  Ladoun
//
//  Created by qw on 17/02/21.
//

import UIKit
import CountableLabel
import SDWebImage

protocol DismissController {
    func dismissView()
}

class MyCartViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var cartTable: UITableView!
    @IBOutlet weak var cartSubtotal: DesignableUILabel!
    
    
    var dismissDelegate:DismissController?
    
    var cartDetail = GetCart()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        APICallingViewController.shared.getCartTotal{ (val) in
            if((val.items?.count ?? 0) > 0){
                self.cartDetail = val
                self.cartSubtotal.text = (self.cartDetail.base_currency_code ?? "") + " \(self.cartDetail.subtotal ?? 0)"
                self.cartTable.reloadData()
            }

        }
    }
    
    
    //MARK: IBActions
    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func checkoutAction(_ sender: Any) {
        self.dismiss(animated: true) {
            self.dismissDelegate?.dismissView()
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
      self.navigationController?.popViewController(animated: true)
    }
    
}

extension MyCartViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cartDetail.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier:"CartTable") as! CartTable
        let val = self.cartDetail.items?[indexPath.row]
        cell.itemName.text = val?.name
        cell.itemImage.sd_setImage(with: URL(string: "https://ladoun.com/pub/media/wysiwyg/pexels-photo-13426091599233514.jpg"), placeholderImage: nil)
        cell.itemCountLabel.text = "\(val?.qty ?? 0)"
        cell.itemPrice.text = "\(self.cartDetail.currency?.global_currency_code ?? "SAR") \(val?.price ?? 0)"
        cell.plusButton={
            var count = Int(cell.itemCountLabel.text ?? "")!
            count += 1
            cell.itemCountLabel.animationType = .pushUp
            cell.itemCountLabel.text = "\(count)"
            
        }
        
        cell.minusButton={
            var count = Int(cell.itemCountLabel.text ?? "")!
            if(count > 1){
                count -= 1
            }
            cell.itemCountLabel.animationType = .pushDown
            cell.itemCountLabel.text = "\(count)"
            //  let amount = Double(count)*(data.item_price ?? 0)
        }
        return cell
    }
}

class CartTable: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemPrice: DesignableUILabel!
    @IBOutlet weak var itemName: DesignableUILabel!
    @IBOutlet weak var itemCountLabel: CountableLabel!
    @IBOutlet weak var itemId: DesignableUILabel!
    @IBOutlet weak var itemExPrice: DesignableUILabel!
    @IBOutlet weak var itemTax: DesignableUILabel!
    @IBOutlet weak var itemQuantity: DesignableUILabel!
    
    
    var minusButton:(()-> Void)? = nil
    var plusButton:(()-> Void)? = nil
    
    //MARK: IBActions
    
    @IBAction func minusAction(_ sender: Any) {
        if let minusButton = self.minusButton{
            minusButton()
        }
    }
    
    @IBAction func plusAction(_ sender: Any) {
        if let plusButton = self.plusButton{
            plusButton()
        }
    }
    
}
