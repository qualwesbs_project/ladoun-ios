//
//  APIManager.swift
//
//  Copyright © 2021 Qualwebs. All rights reserved.
//

import UIKit
import Alamofire


class SessionManager: NSObject {

    static var shared = SessionManager()

    var createWallet: Bool = true

    func methodForApiCalling<T: Codable>(url: String, method: HTTPMethod, parameter: Parameters?, objectClass: T.Type, requestCode: String, userToken: String?, completionHandler: @escaping (T) -> Void) {
        print("URL: \(url)")
        print("METHOD: \(method)")
        print("PARAMETERS: \(parameter)")
        print("TOKEN: \(getHeader(reqCode: requestCode, userToken: userToken))")
        AF.request(url, method: method, parameters: parameter, encoding: JSONEncoding.default, headers:  getHeader(reqCode: requestCode, userToken: userToken), interceptor: nil).responseString { (dataResponse) in
            let statusCode = dataResponse.response?.statusCode
            print("statusCode: ",dataResponse.response?.statusCode)
            print("dataResponse: \(dataResponse)")

            switch dataResponse.result {
            case .success(_):
                var object:T?
                if(statusCode == 200){
                    if(requestCode == U_GUEST_CART_ID){
                      // let data = self.convertDataToObject(response: dataResponse.data, String.self)
                      //  completionHandler(GetCart(response: data) as! T)
                    }else if(requestCode == U_GET_GUEST_CART) {
                        object = self.convertDataToObject(response: dataResponse.data, T.self)
                        completionHandler(object!)
                    }else {
                        object = self.convertDataToObject(response: dataResponse.data, T.self)
                        completionHandler(object!)
                    }
                }
                let errorObject = self.convertDataToObject(response: dataResponse.data, ErrorResponse.self)
                if (statusCode == 200 || statusCode == 201) && object != nil{
                    completionHandler(object!)
                } else if statusCode == 404  {
                   
                  //  NavigationController.shared.showAlertScreen(message: errorObject?.message ?? "")
                    //self.showAlert(msg: errorObject?.message)
                }else if statusCode == 400{

                        if(errorObject?.message != "" || errorObject?.message != nil){
                      
                        }
                       //  NavigationController.shared.showAlertScreen(message: errorObject?.message ?? "")
                       // self.showAlert(msg: errorObject?.message)
                    
                } else {
//                    if(errorObject?.message != "" || errorObject?.message != nil){
//                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:N_SHOW_ERROR_MESSAGE), object: nil, userInfo: ["msg": errorObject?.message ?? ""])
//                    }
                    // NavigationController.shared.showAlertScreen(message: errorObject?.message ?? "")
                     // self.showAlert(msg: errorObject?.message)
                }
                ActivityIndicator.hide()
                break
            case .failure(_):
               // ActivityIndicator.hide()
                let error = dataResponse.error?.localizedDescription
                if error == "The Internet connection appears to be offline." {
                    //Showing error message on alert
                    if(error != "" || error != nil){
                    }
                   //  NavigationController.shared.showAlertScreen(message: error ?? "")
                    //self.showAlert(msg: error)
                      
                } else {
                    //Showing error message on alert
                   // self.showAlert(msg: error)
                    
                }
                break
            }
        }
    }
    
    func makeMultipartRequest(url: String, fileData: Data, param: [String:Any], fileName: String,reqCode: String, completionHandler: @escaping (Any) -> Void) {
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in param {
                if key == "type" {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }else if(key == "video"){
                    multipartFormData.append(value as! URL, withName: key as String)
                }else if(key == "file"){
                    multipartFormData.append(value as! URL, withName: key as! String, fileName: (value as! URL).lastPathComponent, mimeType: (value as! URL).lastPathComponent)
                }else {
                    multipartFormData.append(value as! Data, withName: key as! String, fileName: "image.png", mimeType: "image/png")
                }
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: getHeader(reqCode: reqCode, userToken: nil)).responseString { (encodingResult) in
            print(encodingResult)
//            switch encodingResult.response {
//            case .success(_):
//                encodingResult.response.responseString(completionHandler: { (dataResponse) in
//                    ActivityIndicator.hide()
//
//                    let errorObject = self.convertDataToObject(response: dataResponse.data, Response.self)
//
//                    if dataResponse.response?.statusCode == 200 {
//                        let object = self.convertDataToObject(response: dataResponse.data, UploadImage.self)
//                        completionHandler(object?.response!)
//                    } else {
//                        UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: "Could not upload file", action1Name: "Ok", action2Name: nil)
//                    }
//                })
//                break
//            case .failure(_):
//                //Showing error message on alert
//                UIApplication.shared.keyWindow?.rootViewController?.showAlert(title: "Error", message: "Could not upload file", action1Name: "Ok", action2Name: nil)
//                break
//            }
        }
    }
    

    private func showAlert(msg: String?) {
        UIApplication.shared.keyWindow?.rootViewController?.showAlert(title:"", message: msg, action1Name: "Ok", action2Name: nil)

    }


     func convertDataToObject<T: Codable>(response inData: Data?, _ object: T.Type) -> T? {
        if let data = inData {
            do {
                let decoder = JSONDecoder()
                let decoded = try decoder.decode(T.self, from: data)
                return decoded
            } catch {
                print(error)
            }
        }
        return nil
    }

    
    func getHeader(reqCode: String, userToken: String?) -> HTTPHeaders? {
        var token = UserDefaults.standard.string(forKey: UD_TOKEN)
        if (1==1){
            if(token == nil){
                return nil
            }else {
                return ["Authorization": "Bearer " + token!]
            }
            } else {
                return nil
            }
        }
    }
