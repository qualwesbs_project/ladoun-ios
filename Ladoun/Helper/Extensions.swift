//
//  Extemsions.swift
//
//  Ladoun.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit
import SideMenu
import CoreLocation


extension UIApplication {
    var statusBarView: UIView? {
        if responds(to: Selector("statusBar")) {
            return value(forKey: "statusBar") as? UIView
        }
        return nil
    }
}

extension UIViewController {
    
    func addTransition(direction: CATransitionSubtype,controller: UIViewController) {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = direction
        controller.navigationController?.view.layer.add(transition, forKey: kCATransition)
    }
    
    
    func showAlert(title: String?, message: String?, action1Name: String?, action2Name: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: action1Name, style: .default, handler: nil))
        if action2Name != nil {
            alertController.addAction(UIAlertAction(title: action2Name, style: .default, handler: nil))
        }
        //        if let popoverPresentationController = alertController.popoverPresentationController {
        //            popoverPresentationController.sourceView = self.view
        //            popoverPresentationController.sourceRect = alertController.view.bounds
        //        }
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func showToast(text : String) {
        var toastLabel = UILabel(frame: CGRect.zero)
        toastLabel.font = UIFont(name: "Montserrat-Regular", size: 14.0)
        toastLabel.text = text
        toastLabel.sizeToFit()
        toastLabel.frame.size = CGSize(width: toastLabel.frame.width + 30, height: toastLabel.frame.height + 20)
        if(UIScreen.main.bounds.height > 700){
            toastLabel.center = CGPoint(x: self.view.center.x, y: self.view.frame.maxY + 110)
        }else {
            toastLabel.center = CGPoint(x: self.view.center.x, y: self.view.frame.maxY - 70)
        }
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(1)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 5
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    @objc func back() {
        if let navController = navigationController {
            self.navigationController?.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func menu() {
        SideMenuManager.default.menuFadeStatusBar = false
        guard let sideMenuNavController =  SideMenuManager.defaultManager.menuLeftNavigationController else {
            let sideMenuController = storyboard?.instantiateViewController(withIdentifier: "SideMenuViewController") as! SideMenuViewController
            SideMenuManager.defaultManager.menuLeftNavigationController = UISideMenuNavigationController(rootViewController: sideMenuController)
            SideMenuManager.defaultManager.menuLeftNavigationController?.setNavigationBarHidden(true, animated: false)
            
            present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
            return
        }
        
        present(sideMenuNavController, animated: true, completion: nil)
    }
    
    func pushController(controller: UIViewController) {
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func presentController(controller: UIViewController) {
        DispatchQueue.main.async {
            self.present(controller, animated: true, completion: nil)
        }
    }
    
    //    func popToTabController() {
    //        for controller in (navigationController?.viewControllers)! {
    //            if controller is TabsViewController {
    //                self.navigationController?.popToViewController(controller, animated: true)
    //            }
    //        }
    //    }
    
    func calculateTimeDifference(date1: Int, date2: Int) -> Int{
        let d1 = Date(timeIntervalSince1970: TimeInterval(date1))
        let d2 = Date(timeIntervalSince1970: TimeInterval(date2))
        let diff = d2.timeIntervalSince(d1)
        return Int(diff)
    }
    
    
    func convertTimestampToDate(_ timestamp: Int, to format: String) -> String {
        var myVal = Int()
        var intValue:Int64 = 10000000000
        if(timestamp/Int(truncatingIfNeeded: intValue) == 0){
            myVal = timestamp
        }else {
            myVal = timestamp/1000
        }
        let date = Date(timeIntervalSince1970: TimeInterval(myVal))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        //       var timezone : String?
        //        if(Singleton.shared.userProfileData.timezone != nil){
        //            if(Singleton.shared.userProfileData.timezone!.contains("PST")){
        //                timezone = "GMT-8"
        //            }else if(Singleton.shared.userProfileData.timezone!.contains("MST")){
        //                timezone = "GMT-7"
        //            }else if(Singleton.shared.userProfileData.timezone!.contains("CST")){
        //                timezone = "GMT-6"
        //            }else if(Singleton.shared.userProfileData.timezone!.contains("EST")){
        //               timezone = "GMT-5"
        //            }
        //            dateFormatter.timeZone = TimeZone(abbreviation: timezone ?? "GMT-6")
        //        }else {
        //            dateFormatter.timeZone = TimeZone(abbreviation: "GMT-6")
        //        }
        return dateFormatter.string(from: date)
    }
    
    func openUrl(urlStr: String) {
        let url = URL(string: urlStr)!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func changeColor(string:String,colorString: String,color: UIColor,field:UILabel){
        let main_string = string
        var string_to_color = colorString
        //colorString.font
        var range = (main_string as NSString).range(of: string_to_color)
        
        let attribute = NSMutableAttributedString.init(string: main_string)
        if let font = UIFont(name: "Raleway-SemiBold", size: 20) {
            let fontAttributes = [NSAttributedString.Key.font: font]
            let size = (string_to_color as NSString).size(withAttributes: fontAttributes)
        }
        attribute.addAttribute(NSAttributedString.Key.foregroundColor, value: color , range: range)
        field.attributedText = attribute
    }
    
    func isValidEmail(emailStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: emailStr)
    }
    
    func hasLocationPermission() -> Bool {
        var hasPermission = false
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                hasPermission = false
            case .authorizedAlways, .authorizedWhenInUse:
                hasPermission = true
            }
        } else {
            hasPermission = false
        }
        
        return hasPermission
    }
    
    func callNumber(phoneNumber:String) {
        if let phoneCallURL = URL(string: "telprompt://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                    application.openURL(phoneCallURL as URL)
                }
            }
        }
    }
}


extension String {
    func applyPatternOnNumbers(pattern: String, replacmentCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(encodedOffset: index)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacmentCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
}

extension UIView {
    func setCircularShadow() {
        self.layer.shadowColor = UIColor(red: 111/255, green: 113/255, blue: 121/255, alpha: 0.5).cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.masksToBounds = false
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1
        self.layer.cornerRadius = self.frame.width / 2
    }
    
    func addConstraintsWithFormatString(formate: String, views: UIView...) {
        
        var viewsDictionary = [String: UIView]()
        
        for (index, view) in views.enumerated() {
            let key = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[key] = view
        }
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: formate, options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func setShadow(cornerRadius: CGFloat)
    {
        self.layer.cornerRadius = cornerRadius
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 1
    }
    
}

extension UILabel {
    var isTruncated: Bool {
        guard let labelText = text else {
            return false
        }
        
        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [NSAttributedString.Key.font: font],
            context: nil).size
        
        return labelTextSize.height > bounds.size.height
    }
}

extension UIImageView {
    func changeTint(color: UIColor) {
        let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
    
    static func fromGif(frame: CGRect, resourceName: String) -> UIImageView? {
        guard let path = Bundle.main.path(forResource: resourceName, ofType: "gif") else {
            print("Gif does not exist at that path")
            return nil
        }
        let url = URL(fileURLWithPath: path)
        guard let gifData = try? Data(contentsOf: url),
              let source =  CGImageSourceCreateWithData(gifData as CFData, nil) else { return nil }
        var images = [UIImage]()
        let imageCount = CGImageSourceGetCount(source)
        for i in 0 ..< imageCount {
            if let image = CGImageSourceCreateImageAtIndex(source, i, nil) {
                images.append(UIImage(cgImage: image))
            }
        }
        let gifImageView = UIImageView(frame: frame)
        gifImageView.animationImages = images
        return gifImageView
    }
}



extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}



class UnderlinedLabel: UILabel {
    
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.count)
            let attributedText = NSMutableAttributedString(string: text)
            self.attributedText = NSAttributedString(string: text, attributes:
                                                        [.underlineStyle: NSUnderlineStyle.single.rawValue])
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
            
            //            self.attributedText = NSAttributedString(string: text, attributes:
            //                [.underlineStyle: NSUnderlineStyle.single.rawValue])
        }
    }
}


final class ContentSizedTableView: UITableView {
    override var contentSize:CGSize {
        didSet {
            invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        layoutIfNeeded()
        return CGSize(width: UIView.noIntrinsicMetric, height: contentSize.height)
    }
}




