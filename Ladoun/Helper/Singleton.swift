//
//  Singleton.swift
//  Diamonium
//
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import Foundation
import UIKit
import LocalAuthentication
import Toaster

class Singleton {
    
    static let shared = Singleton()
    
    
    var cartId = "3f96274c73b54bc8d7e1ed7f0f913fe2"//String()
    var cartData = GetCart()
    var cartTotal = GetCart()
    var countryData = [CountryData]()
    var cityData = [CityData]()
    
    var language:String?{
        get {
            if let appLanguage = UserDefaults.standard.value(forKey: UD_APP_LANGUAGE) as? String {
                return appLanguage
            }
            return "en"
        } set {
            saveAppLanguage(withLanguage: newValue!)
        }
    }
    

//    public static func getUser() -> SignupResponse{
//        if let info = UserDefaults.standard.data(forKey: UD_USER_DETAIl){
//            let placeData = info
//            let placeArray = try! JSONDecoder().decode(SignupResponse.self, from: placeData)
//            return placeArray
//        }else {
//            return SignupResponse()
//        }
//    }
    
    func initialiseValues(){
        
    }
    
    
    // -------------------------------- LANGUAGE ----------------------------------
    //Save application lanaguage
    func saveAppLanguage(withLanguage appLang: String)->Void{
        UserDefaults.standard.set(appLang, forKey: UD_APP_LANGUAGE)
        UserDefaults.standard.synchronize()
        changeApplicationLanguage(language: appLang)
    }
    
    //Change Application language : This WIll be used when toggle on off
    func changeApplicationLanguage(language: String) -> Void{
        UserDefaults.standard.set(language, forKey: UD_APP_LANGUAGE)
        
        UserDefaults.standard.synchronize()
      //  setUIViewAccToLanguage(language: language)
    }
    
    func setUIViewAccToLanguage(language: String) {
        if language == "ar" {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        } else {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        bundle = Bundle(path: Bundle.main.path(forResource: (language == "ar" ? language : "Base"), ofType: "lproj")!)
    }
    
    //GET Current Language
    func getCurrentLanguage() -> String {
        var lang = ""
        if let data = UserDefaults.standard.string(forKey: UD_APP_LANGUAGE) as? String {
            lang = language!
          //  setUIViewAccToLanguage(language: lang)
        }
        else{
            print("There is an issue")
        }
        return lang
    }
    
    
    func showToast(text: String?){
        if (ToastCenter.default.currentToast?.text == text) {
             return
            }
        if(UIScreen.main.bounds.height > 700){
            ToastView.appearance().bottomOffsetPortrait = 110
        }else {
            ToastView.appearance().bottomOffsetPortrait = 70
        }
        
            ToastView.appearance().textColor = .white
            ToastView.appearance().backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.7)
            ToastView.appearance().font = UIFont(name: "Montserrat-Medium", size: 17)
           
            Toast(text: text, delay: 0, duration: 2).show()
            Toast(text: text, delay: 0, duration: 2).cancel()
    }

}
