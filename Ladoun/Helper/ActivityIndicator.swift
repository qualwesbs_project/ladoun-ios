//
//  ActivityIndicator.swift
//  Ladoun
//
//  Created by qw on 28/01/21.
//
import UIKit

class ActivityIndicator
{
    static var overlayView = UIView()
    static var activityIndicator = UIActivityIndicatorView()
    
    static func show(view: UIView) {
        DispatchQueue.main.async {
            self.overlayView = view
            self.overlayView.isUserInteractionEnabled = false
            activityIndicator.center = view.center
            activityIndicator.hidesWhenStopped = true
            activityIndicator.startAnimating()
            activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
            activityIndicator.color = primaryColor
            view.addSubview(activityIndicator)
        }
    }
    
    static func showPosition(view: UIView, center: CGPoint) {
           DispatchQueue.main.async {
               self.overlayView = view
               self.overlayView.isUserInteractionEnabled = false
               activityIndicator.center = center
               activityIndicator.hidesWhenStopped = true
               activityIndicator.startAnimating()
               activityIndicator.style = UIActivityIndicatorView.Style.whiteLarge
               activityIndicator.color = primaryColor
               view.addSubview(activityIndicator)
           }
       }
    
    static func hide() {
        overlayView.isUserInteractionEnabled = true
        activityIndicator.stopAnimating()
    }
}

