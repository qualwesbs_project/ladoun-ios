//
//  Model.swift
//
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit

struct MenuObject {
    var image: UIImage?
    var name: String?
}

struct SuccessResponse: Codable {
    var message: String?
    var status: Int?
}

struct ErrorResponse: Codable {
    var message: String?
    var status: Int?
}

struct GetCart: Codable {
    var id: Int?
    var is_active: Bool?
    var is_virtual: Bool?
    var items_count: Int?
    var items_qty: Int?
    var customer: CustomerDetail?
    var items:[CartItemDetail]?
    var orig_order_id: Int?
    var currency: CurrencyDetail?
    var customer_is_guest: Bool?
    var customer_note_notify: Bool?
    var customer_tax_class_id: Int?
    var store_id: Int?
    var grand_total: Double?
    var base_grand_total: Double?
    var subtotal: Double?
    var base_subtotal: Double?
    var discount_amount: Double?
    var base_discount_amount: Double?
    var subtotal_with_discount: Double?
    var base_subtotal_with_discount: Double?
    var shipping_amount: Double?
    var base_shipping_amount: Double?
    var shipping_discount_amount: Double?
    var base_shipping_discount_amount: Double?
    var tax_amount: Double?
    var base_tax_amount: Double?
    var shipping_tax_amount: Double?
    var base_shipping_tax_amount: Double?
    var subtotal_incl_tax: Double?
    var shipping_incl_tax: Double?
    var base_shipping_incl_tax: Double?
    var base_currency_code: String?
  //  var extension_attributes: ExtensionAttribute?
    
}

struct ExtensionAttribute: Codable {
    var shipping_assignments: ShippingAssignment?
}

struct ShippingAssignment: Codable {
    
}

struct ShippingDetail: Codable {
    var address: BillingAddressDetail?
    var items:[CartItemDetail]?
}

struct CurrencyDetail: Codable {
    var CurrencyDetail: String?
    var base_currency_code: String?
    var global_currency_code: String?
    var store_currency_code: String?
    var quote_currency_code: String?
    var store_to_base_rate: Int?
    var store_to_quote_rate: Int?
    var base_to_global_rate: Int?
    var base_to_quote_rate: Int?
}

struct CustomerDetail: Codable {
    var email: String?
    var firstname: String?
    var lastname: String?
}

struct BillingAddressDetail: Codable {
    var id: Int?
    var region: String?
    var region_id: String?
    var region_code: String?
    var country_id: String?
    var telephone: String?
    var postcode: String?
    var city: String?
    var firstname: String?
    var lastname: String?
    var email: String?
    var same_as_billing: Int?
    var save_in_address_book: Int?
}

struct CartItemDetail: Codable {
    var item_id: Int?
    var sku: String?
    var qty: Int?
    var name: String?
    var price: Double?
    var product_type: String?
    var quote_id: String?
    var row_total: Double?
    var base_row_total: Double?
    var row_total_with_discount: Double?
    var tax_amount: Double?
    var base_tax_amount: Double?
    var tax_percent: Double?
    var base_discount_amount: Double?
    var discount_percent: Double?
    var price_incl_tax: Double?
    var base_price_incl_tax: Double?
    var row_total_incl_tax: Double?
    var base_row_total_incl_tax: Double?
}


struct CategoryData:Codable {
    var name: String?
    var image: String?
}

struct GetCountries: Codable {
    var cities = [CountryData]()
}


struct CountryData: Codable {
    var id: String?
    var country_code: String?
    var country_name: String?
    var country_name_ar: String?
}

struct GetCities: Codable {
    var cities = [CityData]()

}

struct CityData: Codable{
    var id: String?
    var region_id: String?
    var code: String?
    var default_name: String?
    var arabic_name: String?
}

struct GetEstimateShipping: Codable {
    var carrier_code: String?
    var method_code: String?
    var carrier_title: String?
    var method_title: String?
    var amount: Double?
    var base_amount: Double?
    var available: Bool?
    var error_message: String?
    var price_excl_tax: Double?
    var price_incl_tax: Double?
}

struct PayfortResponse : Codable {
    var response_code: String?
    var device_id: String?
    var response_message: String?
    var service_command: String?
    var sdk_token: String?
    var signature: String?
    var merchant_identifier: String?
    var access_code: String?
    var language: String?
    var status: String?
}

struct GetCategories: Codable {
    var id: Int?
    var parent_id: Int?
    var name: String?
    var is_active: Bool?
    var position: Int?
    var level: Int?
    var product_count: Int?
    var children_data = [ChildrenData]()
}

struct ChildrenData: Codable {
    var id: Int?
    var parent_id: Int?
    var name: String?
    var is_active: Bool?
    var position: Int?
    var level: Int?
    var product_count: Int?
    var children_data:[ChildrenData]?
}

struct ProductData: Codable {
    var status: String?
    var entity_id: String?
    var attribute_set_id: String?
    var type_id: String?
    var sku: String?
    var cat_index_position: String?
    var brand: String?
    var brand_value: String?
    var color: String?
    var color_value: String?
    var cost: String?
    var dailcolor: String?
    var dailcolor_value: String?
    var description: String?
    var fit: String?
    var fit_value: String?
    var gender_value: String?
    var gift_message_available: String?
    var guarantee: String?
    var hanger: String?
    var has_options: String?
    var image: String?
    var image_label: String?
    var is_featured_product: String?
    var is_in_sale: String?
    var is_in_top_rated: String?
    var is_tailored_product: String?
    var links_exist: String?
    var links_purchased_separately: String?
    var links_title: String?
    var msrp: String?
    var msrp_display_actual_price_type: String?
    var name: String?
    var news_from_date: String?
    var news_to_date: String?
    var price: String?
    var price_type: String?
    var price_view: String?
    var product_label: String?
    var product_label_value: String?
    var product_likes: String?
    var required_options: String?
    var short_description: String?
    var size: String?
    var size_value: String?
    var sku_type: String?
    var small_image: String?
    var small_image_label: String?
    var special_price: String?
    var strap: String?
    var strap_value: String?
    var strapcolor: String?
    var strapcolor_value: String?
    var strap_type: String?
    var strap_type_value: String?
    var swatch_image: String?
    var tax_class_id: String?
    var thumbnail: String?
    var visibility: String?
    var weight: String?
    
    
}


struct DecodedArray: Codable {

    // ***
    // Define typealias required for Collection protocl conformance
    typealias DecodedArrayType = [ProductData]

    // ***
    private var array: DecodedArrayType

    // Define DynamicCodingKeys type needed for creating decoding container from JSONDecoder
    private struct DynamicCodingKeys: CodingKey {

        // Use for string-keyed dictionary
        var stringValue: String
        init?(stringValue: String) {
            self.stringValue = stringValue
        }

        // Use for integer-keyed dictionary
        var intValue: Int?
        init?(intValue: Int) {
            // We are not using this, thus just return nil
            return nil
        }
    }

    init(from decoder: Decoder) throws {

        // Create decoding container using DynamicCodingKeys
        // The container will contain all the JSON first level key
        let container = try decoder.container(keyedBy: DynamicCodingKeys.self)

        // ***
        var tempArray = DecodedArrayType()

        // Loop through each keys in container
        for key in container.allKeys {

            // Decode Student using key & keep decoded Student object in tempArray
            let decodedObject = try container.decode(ProductData.self, forKey: DynamicCodingKeys(stringValue: key.stringValue)!)
            tempArray.append(decodedObject)
        }

        // Finish decoding all Student objects. Thus assign tempArray to array.
        array = tempArray
    }
}

extension DecodedArray: Collection {

    // Required nested types, that tell Swift what our collection contains
    typealias Index = DecodedArrayType.Index
    typealias Element = DecodedArrayType.Element

    // The upper and lower bounds of the collection, used in iterations
    var startIndex: Index { return array.startIndex }
    var endIndex: Index { return array.endIndex }

    // Required subscript, based on a dictionary index
    subscript(index: Index) -> Iterator.Element {
        get { return array[index] }
    }

    // Method that returns the next index when iterating
    func index(after i: Index) -> Index {
        return array.index(after: i)
    }
}
