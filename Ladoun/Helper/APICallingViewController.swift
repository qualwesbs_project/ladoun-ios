//
//  APICallingViewController.swift
//  Ladoun
//
//  Created by qw on 17/02/21.
//

import UIKit
import Alamofire

class APICallingViewController: UIViewController {
    
    static var shared = APICallingViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    func callGusestCartIdAPI(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GUEST_CART_ID, method: .post, parameter: nil, objectClass: GetCart.self, requestCode: U_GUEST_CART_ID, userToken: nil) { (response) in
            // Singleton.shared.guestCardId = response.response ?? ""
            ActivityIndicator.hide()
        }
    }
    
    func getCart(completionHandler: @escaping (GetCart) -> Void){
        if(Singleton.shared.cartData.id == nil){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_GUEST_CART + Singleton.shared.cartId, method: .get, parameter: nil, objectClass: GetCart.self, requestCode: U_GET_GUEST_CART, userToken: nil) { (response) in
                Singleton.shared.cartData = response
                completionHandler(response)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.cartData)
        }
    }
    
    func getCartTotal(completionHandler: @escaping (GetCart) -> Void){
        if(Singleton.shared.cartTotal.items_qty == nil){
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_GUEST_CART + Singleton.shared.cartId + "/totals", method: .get, parameter: nil, objectClass: GetCart.self, requestCode: U_GET_GUEST_CART, userToken: nil) { (response) in
                Singleton.shared.cartTotal = response
                completionHandler(response)
                ActivityIndicator.hide()
            }
        }else {
            completionHandler(Singleton.shared.cartTotal)
        }
    }
    
    func getCountries(completionHandler: @escaping ([CountryData]) -> Void){
        if(Singleton.shared.countryData.count == 0){

            AF.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(Data("getcountries".utf8), withName: "action")
            },
            to: U_GET_COUNTRIES).responseDecodable(of: GetCountries.self) { response in
             let data    = SessionManager.shared.convertDataToObject(response: response.data, GetCountries.self)
                Singleton.shared.countryData = (data!.cities)
                completionHandler(Singleton.shared.countryData)
            }
        }else {
            completionHandler(Singleton.shared.countryData)
        }
    }
    
    func getCities(completionHandler: @escaping ([CityData]) -> Void){
        if(Singleton.shared.cityData.count == 0){

            AF.upload(multipartFormData: { multipartFormData in
                multipartFormData.append(Data("fetchcityforsa".utf8), withName: "action")
            },
            to: U_GET_CITIES).responseDecodable(of: GetCities.self) { response in
             let data    = SessionManager.shared.convertDataToObject(response: response.data, GetCities.self)
                Singleton.shared.cityData = (data!.cities)
                completionHandler(Singleton.shared.cityData)
            }
        }else {
            completionHandler(Singleton.shared.cityData)
        }
    }

    
    func getShippingMethod(param:[String:Any],completionHandler: @escaping ([GetEstimateShipping]) -> Void){
            AF.upload(multipartFormData: { multipartFormData in
                for val in param {
                    
                    multipartFormData.append(Data("\(val.value)".utf8), withName:val.key)
                }
                print("data is:=", multipartFormData)
            },
            to: U_GET_ESTIMATE_SHIPPING).responseDecodable(of: [GetEstimateShipping].self) { response in
             let data    = SessionManager.shared.convertDataToObject(response: response.data, [GetEstimateShipping].self)
                var object = [GetEstimateShipping]()
                for val in data as! [GetEstimateShipping]{
                    if(val.carrier_title == "Aramex" || val.carrier_title == "SMSA"){
                        object.append(val)
                    }
                }
                completionHandler(object)
            }
    }
    
    func getCategories(completionHandler: @escaping (GetCategories) -> Void){
//            AF.upload(multipartFormData: { multipartFormData in
//                multipartFormData.append(Data("getcategories".utf8), withName:"action")
//                multipartFormData.append(Data("english".utf8), withName:"store")
//            },
//            to: U_BASE + U_GET_CATEGORIES).responseDecodable(of: GetCategories.self) { response in
//             let data    = SessionManager.shared.convertDataToObject(response: response.data, GetCategories.self)
//                completionHandler(data!)
//            }
        
        let param:[String:Any] = [
            "action": "getcategories",
            "store": "english"
        ]
        
        SessionManager.shared.methodForApiCalling(url: U_GET_CATEGORIES, method: .post, parameter: param, objectClass: GetCategories.self, requestCode: U_GET_CATEGORIES, userToken: U_GET_CATEGORIES) { (response) in
            print(response)
            completionHandler(response)
        }
    }

}
