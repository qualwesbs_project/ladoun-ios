//
//  Constants.swift
//  Ladoun
//
//  Created by qw on 22/01/21.
//

import Foundation
import UIKit

//COLORS
let primaryColor = UIColor(red: 205/255, green: 163/255, blue: 121/255, alpha: 1) // CDA379
let brownColor = UIColor(red: 61/255, green: 38/255, blue: 30/255, alpha: 1) // 3D261E
let goldColor = UIColor(red: 78/255, green: 78/255, blue: 78/255, alpha: 1) // 4E4E4E
let lightText = UIColor(red: 119/255, green: 119/255, blue: 119/255, alpha: 1) // 777777
let blackColor = UIColor(red: 52/255, green: 52/255, blue: 52/255, alpha: 1) // 343434
let voilet = UIColor(red: 153/255, green: 153/255, blue: 153/255, alpha: 1) // 999999
    
let U_BASE = "https://dev.ladoun.com/rest/V1/"

let U_LOGIN = ""
let U_GUEST_CART_ID = "guest-carts/f"
let U_GET_GUEST_CART = "guest-carts/"
let U_DELETE_GUEST_CART = "guest-carts/"
let U_GET_GUEST_CART_TOTAL = "guest-carts/"
let U_GET_COUNTRIES = "https://dev.ladoun.com/onestep/index/apimethods/getcountries"
let U_GET_CITIES = "https://dev.ladoun.com/onestep/index/apimethods/fetchcityforsa"
let U_GET_ESTIMATE_SHIPPING = "https://dev.ladoun.com/onestep/index/apimethods/estimateshipping"

let U_GET_CATEGORIES = "https://dev.ladoun.com/onestep/index/restapis"

//USER_DEFAULTS

let UD_USER_DETAIl = "user_defaults"
let UD_TOKEN = "user_token"
let UD_APP_LANGUAGE = "app_language"


//sale:- https://ladoun.com/pub/media/wysiwyg/pexels-polina-tankilevitch-3905874_1_1_1599234234.jpg
//pen:- https://ladoun.com/pub/media/wysiwyg/shutterstock_1233776317_1_1599233516.jpg
//button:- https://ladoun.com/pub/media/wysiwyg/shutterstock_1233776317_1_1599233516.jpg
//watch:- https://ladoun.com/pub/media/wysiwyg/pexels-photo-13426091599233514.jpg
//shoes:- https://ladoun.com/pub/media/wysiwyg/pexels-mstudio-12408921611719875.jpg
//accesories:- https://ladoun.com/pub/media/wysiwyg/shutterstock_521355382_1_1599233512.jpg
//ladies watch:- https://ladoun.com/pub/media/wysiwyg/jutta-wilms-a0r2nyk31SE-unsplash_1_1599233495.jpg
